require 'gem2deb/rake/testtask'

if ENV["AUTOPKGTEST_TMP"]
  Gem2Deb::Rake::TestTask.new do |t|
    t.libs = ['test']
    t.test_files = FileList['test/**/*_test.rb'] + FileList['test/**/test_*.rb']
  end
else
  task :default do
    puts "Test needs root, skipping"
  end
end
